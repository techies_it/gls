<?php

/**
 * This class use for return choosed category block template
 *
 * Copyright © 2019 tii.co.in, Inc. All rights reserved.
 * Author: rajan@tii.co.in
 *
 */

namespace Solvature\PopularCategoriesWidget\Block\Adminhtml\Category\Widget;
use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class ChoosedCategory extends Template implements BlockInterface
{

   protected $_template = "widget/choosed_category.phtml";

}