<?php
/**
 * This action  return the category ids
 * Copyright © tii.co.in, Inc. All rights reserved.
 * Author: rajan@tii.co.in
 */
namespace Solvature\PopularCategoriesWidget\Controller\Adminhtml\Widget;

class Index extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\View\Layout
     */
    protected $layout;
    protected $mathRandom;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Widget\Model\Widget\InstanceFactory $widgetFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Math\Random $mathRandom
     * @param \Magento\Framework\Translate\InlineInterface $translateInline
     * @param \Magento\Framework\View\Layout $layout
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Widget\Model\Widget\InstanceFactory $widgetFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Math\Random $mathRandom,
        \Magento\Framework\Translate\InlineInterface $translateInline,
        \Magento\Framework\View\Layout $layout
    ) {
        $this->layout = $layout;
        $this->mathRandom = $mathRandom;
        parent::__construct($context);
    }

    /**
     * Categories chooser Action (Ajax request)
     *
     * @return \Magento\Framework\Controller\Result\Raw
     */
    public function execute()
    {
        $selected = $this->getRequest()->getParam('selected', '');
        $isAnchorOnly =0;// $this->getRequest()->getParam('is_anchor_only', 0);

        /** @var \Magento\Widget\Block\Adminhtml\Widget\Catalog\Category\Chooser $chooser */
        $chooser = $this->layout->createBlock(\Solvature\PopularCategoriesWidget\Block\Adminhtml\Category\Widget\Choosercat::class)
            ->setUseMassaction(true)
            ->setId($this->mathRandom->getUniqueHash('categories'))
          //  ->setIsAnchorOnly($isAnchorOnly)
            ->setSelectedCategories(explode(',', $selected));

        /** @var \Magento\Framework\Controller\Result\Raw $resultRaw */
        $resultRaw = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_RAW);

        return $resultRaw->setContents($chooser->toHtml());
    }
}
