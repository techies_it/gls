<?php

/**
 * This class use for category helper functions
 * Copyright © 2019 tii.co.in, Inc. All rights reserved.
 * Author: rajan@tii.co.in
 *
 */

namespace Solvature\PopularCategoriesWidget\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper {

    /**
     * use for  parent category limit
     */
    const limit = 1;

    /**
     * use for child category
     */
    const child = 3;

    protected $_collectionFactory;
    protected $_storeManager;
    protected $_categoryFactory;

    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager, 
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $collectionFactory, 
        \Magento\Catalog\Model\CategoryFactory $categoryFactory
    ) {
        $this->_collectionFactory = $collectionFactory;
        $this->_storeManager = $storeManager;
        $this->_categoryFactory = $categoryFactory;
    }

    /**
     * @param $categoryIds
     */
    public function getCategories($categoryIds) {
        if (is_string($categoryIds)) {
            $categoryIds = explode(',',$categoryIds);
        }
        
        $result = [];
        
        $mainId = $categoryIds[0];
        $secondaryIds = array_slice($categoryIds, 1);
        
        
        $collection = $this->_collectionFactory->create()
            ->addAttributeToSelect('*')
            ->setStore($this->_storeManager->getStore())
            ->addAttributeToFilter('is_active', '1')
            ->addAttributeToFilter('entity_id', $mainId);
            
        $result[]=$collection->getFirstItem();
        
        $collection = $this->_collectionFactory->create()
            ->addAttributeToSelect('*')
            ->setStore($this->_storeManager->getStore())
            ->addAttributeToFilter('is_active', '1')
            ->addAttributeToFilter('entity_id', ['in', $secondaryIds]);
            
        foreach ($collection as $item) {
            $result[]=$item;
        }
        
        return $result;
    }
}
