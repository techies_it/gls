<?php

/**
 * This class use for date format
 * Copyright © 2019 tii.co.in, Inc. All rights reserved.
 * Author: rajan@tii.co.in
 *
 */
namespace Solvature\DailyCatchWidget\Block\Adminhtml\Form\Element;

use Magento\Framework\Data\Form\Element\Date;

class DateTime extends Date {
    public function getElementHtml() {
        /**
         * return date;
         */
        $this->setDateFormat($this->localeDate->getDateFormat(\IntlDateFormatter::SHORT));
        return parent::getElementHtml();
    }
}