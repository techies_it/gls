<?php
/**
 * Copyright © 2019 tii.co.in, Inc. All rights reserved.
 * Author: rajan@tii.co.in
 * This class use for Daily Catch Widget and give the product collection according
 * to product attribute daily_catch_from, daily_catch_to
 */

namespace Solvature\DailyCatchWidget\Block\Product;
class ProductsList extends \Magento\CatalogWidget\Block\Product\ProductsList
{
    /**
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    public function createCollection()
    {
        /**
         * get current date for filter
         */
        $date = date('Y-m-d 00:00:00');
        $current = $this->formatDate(
            $this->_localeDate->date(new \DateTime($date)),
            \IntlDateFormatter::MEDIUM,
            false
        );
        /**
         *  var $limit
         */

          $limit = $this->getPageSize();

        /**
         * show_pager use for show pagination;
         */

        $show_pager = $this->getShowPager();

        /**
         *  products_count total number of product if need show_pager yes;
         */

        $products_count = $this->getProductsCount();

        /**
         * reset limit if $show_pager set yes
         */
        if ($show_pager == 1 && !empty($products_count)) {
             $limit = $products_count;
        }

//        echo $limit;die;

        /**
         *  array $productSku;
         */
        $productSku = array();
        $collection = $this->productCollectionFactory->create();
        $collection->setVisibility($this->catalogProductVisibility->getVisibleInCatalogIds());
        $collection = $this->_addProductAttributesAndPrices($collection)
            ->addStoreFilter()
            ->setPageSize($this->getPageSize())
            ->setCurPage($this->getRequest()->getParam($this->getData('page_var_name'), 1));
        /**
         * set date filters
         */
        $collection->addAttributeToFilter('daily_catch_from', ['neq' => '']);
        $collection->addAttributeToFilter('daily_catch_to', ['neq' => '']);
        $collection->addAttributeToFilter(
            'daily_catch_from',
            ['lteq' => date('Y-m-d 00:00:00', strtotime($current))]
        )->addAttributeToFilter(
            'daily_catch_to',
            ['gteq' => date('Y-m-d 00:00:00', strtotime($current))]
        );
        $conditions = $this->getConditions();
        $conditions->collectValidatedAttributes($collection);
        $this->sqlBuilder->attachConditionToCollection($collection, $conditions);


        /**
         * get SKU's of products
         */
        if ($collection->getSize() > 0 && $collection->getSize() < $limit) {
            foreach ($collection as $productData) {
                $sku = "'" . $productData->getSku() . "'";
                $productSku[$productData->getSku()] = $sku;
            }
        }
        /**
         * set sku's for list top products
         */
        if (count($productSku) > 0) {
            $product = implode(',', $productSku);
        }
        /**
         * checking is count zero then reset filters query, using default $collection
         */
        if ($collection->getSize() < $limit) {
            $collection = $this->productCollectionFactory->create();
            $collection->setVisibility($this->catalogProductVisibility->getVisibleInCatalogIds());
            $collection = $this->_addProductAttributesAndPrices($collection)
                ->addStoreFilter()
                ->setPageSize($this->getPageSize())
                ->setCurPage($this->getRequest()->getParam($this->getData('page_var_name'), 1));
            $conditions = $this->getConditions();
            $conditions->collectValidatedAttributes($collection);
            $this->sqlBuilder->attachConditionToCollection($collection, $conditions);
        }
        /**
         * set order some product list on top of grid
         */
        if (isset($product) && !empty($product)) {
            $collection->getSelect()->order(new \Zend_Db_Expr("FIELD(sku,$product) desc"));
        }
        $collection->getSelect()->order(new \Zend_Db_Expr("created_at desc"));
        return $collection;
    }
}