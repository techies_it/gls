<?php
/**

 * Copyright © 2019 tii.co.in, Inc. All rights reserved.
 * Author: rajan@tii.co.in
 *
 */


namespace Solvature\DailyCatchWidget\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory /* For Attribute create  */;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;


/**
 * attributes daily_catch_from, daily_catch_to setup
 */
class InstallData implements InstallDataInterface
{
    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    private $eavSetupFactory;
    /**
     * Init
     *
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
        /* assign object to class global variable for use in other class methods */
    }
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {

        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $eavSetup->removeAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'daily_catch_from');
        $eavSetup->removeAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'daily_catch_to');
        /**
         *  Add attributes to the eav_attribute daily_catch_from
         */
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'daily_catch_from',
            [
                'group' => 'Product Details',
                'label' => 'Daily Catch From',
                'type' => 'datetime',
                'input' => 'date',
                'input_renderer' => 'Solvature\DailyCatchWidget\Block\Adminhtml\Form\Element\Datetime',
                'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,
                'class' => 'validate-date',
                'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\Datetime',
                'visible' => true,
                'required' => false,
                'user_defined' => false,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => true,
                'unique' => false,
                'sort_order' => 18,
                'note' => __('Daily catch from date'),


            ]
        );
        /**
         *  Add attributes to the eav_attribute daily_catch_to
         */
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'daily_catch_to',
            [
                'group' => 'Product Details',
                'label' => 'Daily Catch To',
                'type' => 'datetime',
                'input' => 'date',
                'input_renderer' => 'Solvature\DailyCatchWidget\Block\Adminhtml\Form\Element\Datetime',
                'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,
                'class' => 'validate-date',
                'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\Datetime',
                'visible' => true,
                'required' => false,
                'user_defined' => false,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => true,
                'unique' => false,
                'sort_order' => 19,
                'note' => __('Daily catch to date'),


            ]
        );
    }
}