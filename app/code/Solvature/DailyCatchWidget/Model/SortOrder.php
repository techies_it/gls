<?php
/**
 * This class use for sort order Ascending/Descending the product list
 * Copyright © 2019 tii.co.in, Inc. All rights reserved.
 * Author: rajan@tii.co.in
 *
 */

namespace Solvature\DailyCatchWidget\Model;


class SortOrder implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'desc', 'label' => __('Descending')],
            ['value' => 'asc', 'label' => __('Ascending')]
        ];
    }
}